@extends('backend.layouts.backend')

@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ url('/admin') }}">Analytics</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ url('/admin/slides') }}">Slides</a>
        </li>
        <li class="breadcrumb-item active">Create Slide</li>
      </ol>

     

      <!-- Services Create Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> {{ __('Create Slide') }}
        </div>

        <div class="card-body">  

<!-- Form   -->

    <form method="POST" action="{{ route('slides.store') }}" enctype="multipart/form-data" >
        @csrf

      <div class="form-group row">
          <label for="heading" class="col-md-4 col-form-label text-md-right">{{ __('Heading') }}</label>

          <div class="col-md-6">
              <input id="heading" type="text" class="form-control @error('heading') is-invalid @enderror" name="heading" value="{{ old('heading') }}" required autocomplete="heading" autofocus>

              @error('heading')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row">
       
        <div class="col-md-6 offset-md-4">
            <img id="image_file" src=" {{ asset('storage/slides/1900x1080.png') }} " width="100%" height="200" > 
        </div>  
      </div>  


      <div class="form-group row">
          <label for="upload_img" class="col-md-4 col-form-label text-md-right">{{ __('Image for slide') }}  
          </label>

          <div class="col-md-6">

              <input id="upload_img" type="file" class="form-control-file"  name="upload_img" required autocomplete="upload_img">

                  @error('upload_img')
                    <p><span style="margin-top: .25rem; font-size: .875rem; color: #dc3545;">
                      <strong>{{ $message }}</strong>
                    </span></p>
                  @enderror
              
          </div> 
      </div>
 

      <div class="form-group row">
          <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

          <div class="col-md-6">
              <textarea  id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="3" required >{{ old('description') }}</textarea>

              @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

       <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
            </div>
        </div>

    </form>

       
        </div> <!-- /.card-body -->

        <div class="card-footer small text-muted">
              Updated yesterday at 11:59 PM 
        </div> <!-- /.card-footer -->
      </div><!-- /.card -->


    </div>
  </div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  
@endsection

    
@push('script_Change_Image')

  <script src="{{asset('backend')}}/js/change-image.js"></script>
  
@endpush

		