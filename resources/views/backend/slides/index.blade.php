@extends('backend.layouts.backend')

@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('analytics')  }}">Analytics</a>
        </li>
        <li class="breadcrumb-item active">Slides</li>
      </ol>

     

      <!-- Services DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Slides
        </div>
        <div class="card-body">      

          <div class="table-responsive">
            <table id="TableAddDelButton" class="table table-bordered"  width="100%" cellspacing="0"> 
              <tbody>
                <tr>
                  <td>
                    <a id="btnNew" class="btn btn-primary" href=" {{ route('slides.create') }} "  title="New" role="button"><i class="fa fa-plus-circle fa-lg " aria-hidden="true"></i> New</a>
                  </td>    
                </tr>  
              </tbody>
            </table>          
          </div> 


          <div class="table-responsive">

             <table id="exampleSlide" class="table table-hover table-bordered"  width="100%" cellspacing="0"> 
             
            <thead>
              <tr>
                <th><input  type="checkbox" value="" id="head_check"></th> 
                <th class="d-none">ID</th>
                <th>Image</th>
                <th>Heading</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th><input  type="checkbox" value="" id="foot_check"></th>
                <th class="d-none">ID</th>
                <th>Image</th>
                <th>Heading</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </tfoot>
            <tbody>

            @if(isset($slides) && ($slides->count()) > 0 )
              @foreach ($slides as $slide)

              <tr id="slide_{{$slide->id}}">
                <td><input  type="checkbox" value="" id="row_check_1"></td>
                <td class="d-none"> {{ $slide->id }} </td>
                <td>  <img src=" {{ asset( $slide->slide_img ) }} "  width="150" height="75">   </td>
                <td> {{ $slide->heading }} </td>
                <td> {{ $slide->description }} </td>
                
                <td> 
                  <a id="btnEdit" class="btn btn-success mr-1" href="{{ route('slides.edit',['slide ' => $slide]) }}" title="Edit" role="button"><i class="fa fa-edit fa-lg" aria-hidden="true"></i> Edit</a>

                  <button  id="btnDelete" type="button" class="btn btn-danger  ml-1"  title="Delete" value="{{ $slide->id  }}"  data-url=" {{ route('slides.destroy',['slide ' => $slide]) }}" > <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
                  </button>

                </td>
              </tr>

              @endforeach

             @else 

              <tr>
                <td><input  type="checkbox" value="" id="row_check_2"></td>
                <td class="d-none">No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
               <td> 
                  <a id="btnEdit" class="btn btn-success mr-1 disabled" href="" title="Edit" role="button"><i class="fa fa-edit fa-lg" aria-hidden="true"></i> Edit</a>

                  <button  id="btnDelete" type="button" disabled class="btn btn-danger  ml-1"  title="Delete" value=""  data-url="" > <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
                  </button>
                </td>

             @endif 


              
             </tbody> 
           </table>


          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>   



      <meta name="_token" content="{!! csrf_token() !!}" />

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
  
@endsection

    
@push('script_Slides')



  <script type="text/javascript">

// DESC  //  ASC
  $(document).ready(function(){
      
      var selected = [];

     var table = $("#exampleSlide1").DataTable({

      "order": [
          [ 1, "desc" ]
        ],
       "columnDefs": [
          { "searchable": false, "targets": 0 },
          { "orderable": false, "targets": 0 },
          { "searchable": false, "targets": 2 },
          { "orderable": false, "targets": 2 },
          { "searchable": false, "targets": 5 },
          { "orderable": false, "targets": 5 },
        ],
         "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
          "language": {
          //  "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Ukrainian.json"
        }         
      });

   //  console.log('table = ',table);

//=====================

     $('#exampleSlide tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        //var data = table.row( this );

      console.log('table_data = ',data);


        //alert( 'You clicked on '+data[1]+'\'s row' );
    } );

//==============
   

    //delete Slide and remove it from list
    $(document).on('click','#btnDelete',function(){
        
        var slide_id = $(this).val();
        var url = $(this).data("url");

        console.log(url, slide_id);
        
    //     debugger;
       
        if(confirm("ВЫ ХОТИТЕ УДАЛИТЬ ЗАПИСЬ "+ slide_id + " ?"))
        {
             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: url,
                processData : false,
                contentType : false,
                success: function (data) {
                    console.log(data);
                    $("#slide_" + slide_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
         }; // end if
       });


    

  });
    
  </script>

  
@endpush

		