<!-- Page Header -->
<header class="masthead" style="background-image: url('{{asset(config('set_space.theme'))}}/img/{{ $post->heading_image }}')">
 <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-heading">
         <h1>{{ $post->title }}</h1>
		 @if(isset($post->subtitle ))
         <span class="subheading"> {{ $post->subtitle }}</span>
	     @endif
		  <span class="meta">Posted by
            <a href="{{ route('home') }}">{{ config('set_space.name') }}</a>
           on {{$post->created_at->format(config('set_space.date_format')) }} </span>
        </div>
       </div>
    </div>
 </div>
</header>

<!-- END Page Header -->
    