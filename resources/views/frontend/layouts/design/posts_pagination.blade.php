
	<section class="page_pagination">
	
	<nav aria-label="Page navigation">
      <div class="container">  
        <div class="row"> 
	
	@if ($paginator->hasPages())
    <ul class="pagination pagination-lg mx-auto mb-5">
       
	   {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
			
           <li class="disabled ">
				<a class="btn btn-secondary disabled" aria-disabled="true" href="{{ $paginator->previousPageUrl() }}" rel="prev"> &laquo; Previous Posts</a>
				
		   </li>
        @else
            <li class="page-item intro-button px-lg-1">
				<a class=" btn btn-primary" href="{{ $paginator->previousPageUrl() }}" rel="prev"> &laquo; Previous Posts</a>
			</li>
        @endif
		
			
		

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item intro-button px-lg-1 disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item intro-button px-lg-1 active">
							<span class="btn btn-primary active">{{ $page }} <span class="sr-only"></span></span>
						</li>
                    @else
                        <li class="page-item intro-button px-lg-1" >
					        <a class="btn btn-primary" href="{{ $url }}">{{ $page }}</a>
						</li>
                    @endif
                @endforeach
            @endif
        @endforeach

		
		 
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item intro-button px-lg-1" >
				<a class=" btn btn-primary" href="{{ $paginator->nextPageUrl() }}" rel="next">Next Posts &raquo;</a>
			</li>
        @else
			
            <li class="disabled">
			   <a class=" btn btn-secondary disabled" aria-disabled="true" href="{{ $paginator->nextPageUrl() }}" rel="next">Next Posts  &raquo;</a>
			</li>
        @endif
    </ul>
@endif

		</div>
	  </div>
	</nav>
	
	 <!-- Page navigation -->
      
	
    </section>

    