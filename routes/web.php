<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {  D:\laragon\www\greatinfo.loc\resources\views\frontend\front_page
//     return view('welcome');
// });


// Route::get('/', function () {
//     return view('frontend.front_page.index');
// });

Route::get('/', 'Frontend\HomeController@index')->name('home');


// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	
	//Analytics
	Route::get('/', 'Backend\AnalyticsController@index')->name('analytics');

	//Slides
	Route::resource('slides','Backend\SlidesController');

	//Services
	Route::resource('services','Backend\ServicesController');


});	





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home1');
