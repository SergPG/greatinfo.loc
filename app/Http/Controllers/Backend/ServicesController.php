<?php

namespace App\Http\Controllers\Backend;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Backend\BackendController;

class ServicesController extends BackendController
{
    

    protected  $path_image = '/public/services';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $services = Service::all(); 

        // dd( $services->count() );

         return view('backend.services.index')->with([
                         'services' => $services
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return view('backend.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
             $service = new Service;
      
            $this->validate($request, [
                'heading' => 'required|max:255',
                'upload_img' => 'required|image',
                'description' => 'required',
              ]);

            //-------
            $upload_img = $request->file('upload_img'); 
            $ext_img = $upload_img->getClientOriginalExtension();

            $new_name_img = str_random(20).'.'.$ext_img;

            $storage_img = $upload_img->storeAs($this->path_image,$new_name_img);
             
            $url_storage_img = Storage::url($storage_img);

            //-------------

            $service->heading = e($request->heading);
            $service->description = e($request->description);
            $service->thumbnail = $url_storage_img;

          $service->save(); 
           
         return redirect('admin/services')->with('status', 'Slide Created!');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
        return view('backend.services.edit')->with([
                         'service' => $service
                     ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
               $old_url_img = $service->thumbnail;

               $pos =  strpos($old_url_img, '/',1); 
               // "/storage/slides/slide_17.jpg"

               $puth = substr($old_url_img,  ($pos + 1));
               // "slides/slide_17.jpg"

                $exists = Storage::disk('public')->exists( $puth );

                 if( $exists ){
                    $delfiles = Storage::disk('public')->delete( $puth );
                  }
   

      $result  = $service->delete();  

      return response()->json($result);





    }
}
