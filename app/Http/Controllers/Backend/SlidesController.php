<?php

namespace App\Http\Controllers\Backend;

use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Http\Controllers\Backend\BackendController;

class SlidesController extends BackendController
{
  
    protected  $path_image = '/public/slides';
    protected  $status_edit = true;



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $slides = Slide::all(); 

         return view('backend.slides.index')->with([
                         'slides' => $slides
                     ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        $this->status_edit = false;
        return view('backend.slides.create')->with([
                         'status_edit' => $this->status_edit
                     ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
            $slide = new Slide;
      
            $this->validate($request, [
                'heading' => 'required|max:255',
                'upload_img' => 'required|image',
                'description' => 'required',
              ]);

            //-------
            $upload_img = $request->file('upload_img'); 
            $ext_img = $upload_img->getClientOriginalExtension();

            $slide_img = str_random(20).'.'.$ext_img;

            $url_img = $upload_img->storeAs($this->path_image,$slide_img);
             
            $url_slide_img = Storage::url($url_img);

            //-------------

            $slide->heading = e($request->heading);
            $slide->description = e($request->description);
            $slide->slide_img = $url_slide_img;

          $slide->save(); 
           
         return redirect('admin/slides')->with('status', 'Slide Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function show(Slide $slide)
    {
        //
        echo "string";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {

         $this->status_edit = true;
         return view('backend.slides.edit')->with([
                         'slide' => $slide,
                         'status_edit' => $this->status_edit
                     ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slide $slide)
    {
        //
        
            $this->validate($request, [
                'heading' => 'required|max:255',
                'upload_img' => 'image',
                'description' => 'required',
              ]);

            //-------
            //   dd($old_url_img);

           if ($request->hasFile('upload_img')) {

               $old_url_img = $slide->slide_img;

               $pos =  strpos($old_url_img, '/',1); 
               // "/storage/slides/slide_17.jpg"

               $puth = substr($old_url_img,  ($pos + 1));
               // "slides/slide_17.jpg"

                $exists = Storage::disk('public')->exists( $puth );

                 if( $exists ){
                    $delfiles = Storage::disk('public')->delete( $puth );
                  }
   

                $upload_img = $request->file('upload_img'); 
                $ext_img = $upload_img->getClientOriginalExtension();

                $slide_img = str_random(20).'.'.$ext_img;

                $url_img = $upload_img->storeAs($this->path_image,$slide_img);
                 
                $url_slide_img = Storage::url($url_img);

                $slide->slide_img = $url_slide_img;
            } 
            //---END if()  ---------

            $slide->heading = e($request->heading);
            $slide->description = e($request->description);
            
         $slide->save();     

         return redirect('admin/slides')->with('status', 'Slide Update!'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        //
           //

               $old_url_img = $slide->slide_img;

               $pos =  strpos($old_url_img, '/',1); 
               // "/storage/slides/slide_17.jpg"

               $puth = substr($old_url_img,  ($pos + 1));
               // "slides/slide_17.jpg"

                $exists = Storage::disk('public')->exists( $puth );

                 if( $exists ){
                    $delfiles = Storage::disk('public')->delete( $puth );
                  }
   
       
     // dd($slide)

      $result  = $slide->delete();  

      return response()->json($result);



    }
}
