<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slide;

class HomeController extends Controller
{
    //

    public function index()
    {
        //
        $slides = Slide::all(); 

         return view('frontend.front_page.index')->with([
                         'slides' => $slides
                     ]);

    }



}
