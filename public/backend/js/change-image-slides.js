// 

  $(document).ready(function(){
      
    //==========  File  =======

    var files; // переменная. будет содержать данные файлов

      // заполняем переменную данными файлов, при изменении значения file поля
      $('input[type=file]').on('change', function(){
        files = this.files;
      //--------------------

          var reader = new FileReader();
          
           reader.onload = function(event) {
              var dataUri = event.target.result;

          //    <img id="image_file">          
                $('#image_file').attr('src', dataUri);
                console.log('OK !');
            };
 
            reader.onerror = function(event) {
                console.error("Файл не может быть прочитан! код " + event.target.error.code);
            };
             
            reader.readAsDataURL(files[0]);

            //------------------------
        });

       //========== END File  =======
  });
    
